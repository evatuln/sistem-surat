<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratmasuk extends Model
{
    protected $table = "suratmasuk";

    protected $fillable = [
        'tgl_surat', 'tgl_terima', 'perihal', 'jenis_id', 'instansi_id', 'bagian_id', 'user_id', 'dokumen'
    ];
}
