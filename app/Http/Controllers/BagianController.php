<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Bagian;
use Auth;

class BagianController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        return view('bagian.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama_bagian' => 'required|unique:bagian',
        ]);

        $bagian = new Bagian;
        $bagian->nama_bagian = $request["nama_bagian"];
        $bagian->save();

        return redirect('/bagian')->with('success', 'Bagian Berhasil Disimpan!');
        
    }


    public function index()
    {
        $bagian = Bagian::all();
        return view('bagian.index', compact('bagian'));
    }

   
    public function show($id)
    {
        $bagian = Bagian::find($id);
        return view('bagian.show', compact('bagian'));
    }

   
    public function edit($id)
    {
        $bagian = Bagian::find($id);
        return view('bagian.edit', compact('bagian'));
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_bagian' => 'required',
        ]);

        $update = Bagian::where('id', $id)->update([
            "nama_bagian" => $request["nama_bagian"],
        ]);

        return redirect('/bagian')->with('success', 'Berhasil Update Bagian!');
    }

   
    public function destroy($id)
    {
        Bagian::destroy($id);
        return redirect('/bagian')->with('success', 'Berhasil Update dihapus!');
    }
}
