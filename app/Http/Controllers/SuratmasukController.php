<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Suratmasuk;
use Auth;

class SuratmasukController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
        public function create()
    {
        return view('suratmasuk.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tgl_surat' => 'date',
            'tgl_terima' => 'date',
            'perihal' => 'required',
            'jenis_id' => 'numeric',
            'instansi_id' => 'numeric',
            'bagian_id' => 'numeric',
            'dokumen' => 'required',
        ]);
       
        $reqSurat = $request->all();
        $reqSurat['user_id']= Auth::id();
        SuratMasuk::create($reqSurat);
        //dd($suratmasuk);
        return redirect('/suratmasuk')->with('success', 'Surat Masuk Berhasil Disimpan!');
    }


    public function index()
    {
        $suratmasuk = Suratmasuk::all();
        return view('suratmasuk.index', compact('suratmasuk'));
    }

    public function show($id)
    {
        $suratmasuk = Suratmasuk::find($id);
        return view('suratmasuk.show', compact('suratmasuk'));
    }

    public function edit($id)
        {
            $suratmasuk = Suratmasuk::find($id);
            return view('suratmasuk.edit', compact('suratmasuk'));
        }

        public function update($id, Request $request)
            {
                $reqSurat = $request->validate([
                    'tgl_surat' => 'date',
                    'tgl_terima' => 'date',
                    'perihal' => 'required',
                    'jenis_id' => 'numeric',
                    'instansi_id' => 'numeric',
                    'bagian_id' => 'numeric',
                    'dokumen' => 'required',
                ]);

                $reqSurat['user_id']= Auth::id();
                SuratMasuk::where('id', $id)->update($reqSurat);
                //dd($suratmasuk);

                return redirect('/suratmasuk')->with('success', 'Berhasil Update Surat Masuk!');
            }

            
        public function destroy($id)
        {
            Suratmasuk::destroy($id);
            return redirect('/suratmasuk')->with('success', 'Berhasil Update dihapus!');
        }
}
