<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Instansi;
use Auth;

class InstansiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    

    public function create()
    {
        return view('instansi.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'nama_instansi' => 'required|unique:instansi',
        ]);

        $instansi = new Instansi;
        $instansi->nama_instansi = $request["nama_instansi"];
        $instansi->save();

        return redirect('/instansi')->with('success', 'Instansi Berhasil Disimpan!');
        
    }


    public function index()
    {
        $instansi = Instansi::all();
        return view('instansi.index', compact('instansi'));
    }

   
    public function show($id)
    {
        $instansi = Instansi::find($id);
        return view('instansi.show', compact('instansi'));
    }

   
    public function edit($id)
    {
        $instansi = Instansi::find($id);
        return view('instansi.edit', compact('instansi'));
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_instansi' => 'required',
        ]);

        $update = Instansi::where('id', $id)->update([
            "nama_instansi" => $request["nama_instansi"],
        ]);

        return redirect('/instansi')->with('success', 'Berhasil Update Instansi!');
    }

   
    public function destroy($id)
    {
        Instansi::destroy($id);
        return redirect('/instansi')->with('success', 'Berhasil Update dihapus!');
    }
}
