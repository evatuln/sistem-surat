<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
        public function create()
    {
        return view('profile.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap' => 'required',
            'foto' => 'required',
            'user_id' => 'numeric',
        ]);
       
        $reqProfile = $request->all();
        Profile::create($reqProfile);
        //dd($profile);
        return redirect('/profile')->with('success', 'Profile Keluar Berhasil Disimpan!');
    }


    public function index()
    {
        $profile = Profile::all();
        return view('profile.index', compact('profile'));
    }

    public function show($id)
    {
        $profile = Profile::find($id);
        return view('profile.show', compact('profile'));
    }

    public function edit($id)
        {
            $profile = Profile::find($id);
            return view('profile.edit', compact('profile'));
        }

        public function update($id, Request $request)
            {
                $request->validate([
                    'nama_lengkap' => 'required',
                    'foto' => 'required',
                    'user_id' => 'numeric',
                ]);

                $update = Profile::where('id', $id)->update([
                    "nama_lengkap" => $request["nama_lengkap"],
                    "foto" => $request["foto"],
                    "user_id" => $request["user_id"],
                ]);

                return redirect('profile')->with('success', 'Berhasil Update Profile!');
            }

            
        public function destroy($id)
        {
            Profile::destroy($id);
            return redirect('/profile')->with('success', 'Berhasil Update dihapus!');
        }
}
