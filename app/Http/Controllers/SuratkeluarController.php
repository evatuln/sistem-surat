<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Suratkeluar;
use Auth;

class SuratkeluarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
        public function create()
    {
        return view('suratkeluar.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tgl_surat' => 'date',
            'perihal' => 'required',
            'jenis_id' => 'numeric',
            'instansi_id' => 'numeric',
            'bagian_id' => 'numeric',
            'dokumen' => 'required',
        ]);
       
        $reqSurat = $request->all();
        $reqSurat['user_id']= Auth::id();
        SuratKeluar::create($reqSurat);
        //dd($suratkeluar);
        return redirect('/suratkeluar')->with('success', 'Surat Keluar Berhasil Disimpan!');
    }


    public function index()
    {
        $suratkeluar = Suratkeluar::all();
        return view('suratkeluar.index', compact('suratkeluar'));
    }

    public function show($id)
    {
        $suratkeluar = Suratkeluar::find($id);
        return view('suratkeluar.show', compact('suratkeluar'));
    }

    public function edit($id)
        {
            $suratkeluar = Suratkeluar::find($id);
            return view('suratkeluar.edit', compact('suratkeluar'));
        }

        public function update($id, Request $request)
            {
                $reqSurat = $request->validate([
                    'tgl_surat' => 'date',
                    'perihal' => 'required',
                    'jenis_id' => 'numeric',
                    'instansi_id' => 'numeric',
                    'bagian_id' => 'numeric',
                    'dokumen' => 'required',
                ]);

                $reqSurat['user_id']= Auth::id();
                Suratkeluar::where('id', $id)->update($reqSurat);
                //dd($suratkeluar);

                return redirect('/suratkeluar')->with('success', 'Berhasil Update Surat Keluar!');
            }

            
        public function destroy($id)
        {
            Suratkeluar::destroy($id);
            return redirect('/suratkeluar')->with('success', 'Berhasil Update dihapus!');
        }
}
