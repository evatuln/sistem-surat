<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Jenissurat;
use Auth;

class JenissuratController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function create()
    {
        return view('jenissurat.create');
    }

    
    public function store(Request $request)
    {
        $request->validate([
            'jenis_surat' => 'required|unique:jenissurat',
        ]);

        $jenissurat = new Jenissurat;
        $jenissurat->jenis_surat = $request["jenis_surat"];
        $jenissurat->save();

        return redirect('/jenissurat')->with('success', 'Jenissurat Berhasil Disimpan!');
        
    }


    public function index()
    {
        $jenissurat = Jenissurat::all();
        return view('jenissurat.index', compact('jenissurat'));
    }

   
    public function show($id)
    {
        $jenissurat = Jenissurat::find($id);
        return view('jenissurat.show', compact('jenissurat'));
    }

   
    public function edit($id)
    {
        $jenissurat = Jenissurat::find($id);
        return view('jenissurat.edit', compact('jenissurat'));
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'jenis_surat' => 'required',
        ]);

        $update = Jenissurat::where('id', $id)->update([
            "jenis_surat" => $request["jenis_surat"],
        ]);

        return redirect('/jenissurat')->with('success', 'Berhasil Update Jenissurat!');
    }

   
    public function destroy($id)
    {
        Jenissurat::destroy($id);
        return redirect('/jenissurat')->with('success', 'Berhasil Update dihapus!');
    }
}
