<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratkeluar extends Model
{
    protected $table = "suratkeluar";

    protected $fillable = [
        'tgl_surat', 'perihal', 'jenis_id', 'instansi_id', 'bagian_id', 'user_id', 'dokumen'
    ];
}
