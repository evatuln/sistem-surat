<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bagian extends Model
{
    protected $table = "bagian";

    protected $fisible = ["nama_bagian"];
}
