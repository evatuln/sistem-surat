@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i>  Data Profile
@endsection

@section('judul')
    
<a href="{{ route('profile.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i> Tambah</a>
@endsection


@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success') }}
    </div>
@endif

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama Lengkap</th>
                <th scope="col">Foto</th>
                <th scope="col">ID User</th>
                <th scope="col" >Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($profile as $key=>$profile)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$profile->nama_lengkap}}</td>
                        <td>{{$profile->foto}}</td>
                        <td>{{$profile->user_id}}</td>
                        <td style="display: flex;">
                            <form action="/profile/{{$profile->id}}" method="POST">
                                <a href= " {{ route('profile.show', ['profile' => $profile->id])}}" class="btn btn-info">Show</a>
                                <a href="/profile/{{$profile->id}}/edit" class="btn btn-primary">Edit</a>
                                
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection