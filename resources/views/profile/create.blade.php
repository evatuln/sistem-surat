@extends('layout.master')

@section('title')
   Data Profile User
@endsection

@section('judul')
    Tambah Profile
@endsection


@section('content')
<div>
        <form action="/profile" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Lengkap</label>
                <input type="text" class="form-control" name="nama_lengkap" id="title" placeholder="Masukkan Nama Lengkap">
                @error('nama_lengkap')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Foto</label>
                <input type="text" class="form-control" name="foto" id="title" placeholder="Masukkan Foto">
                @error('foto')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID User</label>
                <input type="text" class="form-control" name="user_id" id="title" placeholder="Masukkan ID User">
                @error('user_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
                
        </form>
</div>
@endsection