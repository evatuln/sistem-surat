@extends('layout.master')

@section('title')
    Data Profile
@endsection

@section('judul')
    Edit Data dengan ID : {{$profile->id}}
@endsection

@section('content')
<div>
        <form action="/profile/{{$profile->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Lengkap</label>
                <input type="text" class="form-control" value="{{$profile->nama_lengkap}}" name="nama_lengkap" id="title" placeholder="Masukkan Tanggal Surat">
                @error('nama_lengkap')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Foto</label>
                <input type="text" class="form-control" value="{{$profile->foto}}" name="foto" id="title" placeholder="Masukkan Foto">
                @error('foto')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID User</label>
                <input type="text" class="form-control" value="{{$profile->user_id}}" name="user_id" id="title" placeholder="Masukkan ID User">
                @error('user_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
</div>
@endsection