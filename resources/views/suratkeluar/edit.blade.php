@extends('layout.master')

@section('title')
    Data Surat Keluar
@endsection

@section('judul')
    Edit Data dengan ID : {{$suratkeluar->id}}
@endsection

@section('content')
<div>
        <form action="/suratkeluar/{{$suratkeluar->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Tanggal Surat</label>
                <input type="date" class="form-control" value="{{$suratkeluar->tgl_surat}}" name="tgl_surat" id="title" placeholder="Masukkan Tanggal Surat">
                @error('tgl_surat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Perihal</label>
                <input type="text" class="form-control" value="{{$suratkeluar->perihal}}" name="perihal" id="title" placeholder="Masukkan Perihal">
                @error('perihal')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID Jenis</label>
                <input type="text" class="form-control" value="{{$suratkeluar->jenis_id}}" name="jenis_id" id="title" placeholder="Masukkan ID Jenis">
                @error('jenis_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID Instansi</label>
                <input type="text" class="form-control" value="{{$suratkeluar->instansi_id}}" name="instansi_id" id="title" placeholder="Masukkan ID Instansi">
                @error('instansi_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID Bagian</label>
                <input type="text" class="form-control" value="{{$suratkeluar->bagian_id}}" name="bagian_id" id="title" placeholder="Masukkan ID Bagian">
                @error('bagian_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Dokumen</label>
                <input type="text" class="form-control" value="{{$suratkeluar->dokumen}}" name="dokumen" id="title" placeholder="Masukkan Dokumen">
                @error('dokumen')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
</div>
@endsection