@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i>  Data Surat Keluar
@endsection

@section('judul')
    
<a href="{{ route('suratkeluar.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i> Tambah</a>
@endsection


@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success') }}
    </div>
@endif

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tanggal Surat</th>
                <th scope="col">Perihal</th>
                <th scope="col">ID Jenis</th>
                <th scope="col">ID Instansi</th>
                <th scope="col">ID Bagian</th>
                <th scope="col">ID User</th>
                <th scope="col">Dokumen</th>
                <th scope="col" >Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($suratkeluar as $key=>$suratkeluar)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$suratkeluar->tgl_surat}}</td>
                        <td>{{$suratkeluar->perihal}}</td>
                        <td>{{$suratkeluar->jenis_id}}</td>
                        <td>{{$suratkeluar->instansi_id}}</td>
                        <td>{{$suratkeluar->bagian_id}}</td>
                        <td>{{$suratkeluar->user_id}}</td>
                        <td>{{$suratkeluar->dokumen}}</td>
                        <td style="display: flex;">
                            <form action="/suratkeluar/{{$suratkeluar->id}}" method="POST">
                                <a href= " {{ route('suratkeluar.show', ['suratkeluar' => $suratkeluar->id])}}" class="btn btn-info">Show</a>
                                <a href="/suratkeluar/{{$suratkeluar->id}}/edit" class="btn btn-primary">Edit</a>
                                
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection