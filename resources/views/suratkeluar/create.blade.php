@extends('layout.master')

@section('title')
   Data Surat Keluar
@endsection

@section('judul')
    Tambah Data Surat Keluar
@endsection


@section('content')
<div>
        <form action="/suratkeluar" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Tanggal Surat</label>
                <input type="date" class="form-control" name="tgl_surat" id="title" placeholder="Masukkan Tanggal Surat">
                @error('tgl_surat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Perihal</label>
                <input type="text" class="form-control" name="perihal" id="title" placeholder="Masukkan Perihal">
                @error('perihal')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID Jenis</label>
                <input type="text" class="form-control" name="jenis_id" id="title" placeholder="Masukkan ID Jenis">
                @error('jenis_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID Instansi</label>
                <input type="text" class="form-control" name="instansi_id" id="title" placeholder="Masukkan ID Instansi">
                @error('instansi_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">ID Bagian</label>
                <input type="text" class="form-control" name="bagian_id" id="title" placeholder="Masukkan ID Bagian">
                @error('bagian_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Dokumen</label>
                <input type="text" class="form-control" name="dokumen" id="title" placeholder="Masukkan Dokumen">
                @error('dokumen')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
</div>
@endsection