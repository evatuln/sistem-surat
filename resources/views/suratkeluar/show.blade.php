@extends('layout.master')

@section('title')
   Show : {{$suratkeluar->tgl_terima}}
@endsection

@section('judul')
    ID Data = {{$suratkeluar->perihal}}
@endsection


@section('content')
    <p> Tanggal Surat   : {{$suratkeluar->tgl_surat}}</p>
    <p> Perihal  : {{$suratkeluar->perihal}}</p>
    <p> ID Jenis   : {{$suratkeluar->jenis_id}}</p>
    <p> ID Instansi   : {{$suratkeluar->instansi_id}}</p>
    <p> ID Bagian   : {{$suratkeluar->bagian_id}}</p>
    <p> ID User   : {{$suratkeluar->user_id}}</p>
    <p> Dokumen   : {{$suratkeluar->dokumen}}</p>

@endsection 