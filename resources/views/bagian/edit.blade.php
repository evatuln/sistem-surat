@extends('layout.master')

@section('title')
    Data Bagian
@endsection

@section('judul')
    Edit Data dengan ID : {{$bagian->id}}
@endsection

@section('content')
<div>
        <form action="/bagian/{{$bagian->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Bagian</label>
                <input type="text" class="form-control" value="{{$bagian->nama_bagian}}" name="nama_bagian" id="title" placeholder="Masukkan Nama Bagian">
                @error('nama_bagian')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
</div>
@endsection