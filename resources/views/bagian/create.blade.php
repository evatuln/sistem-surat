@extends('layout.master')

@section('title')
   Data Bagian
@endsection

@section('judul')
    Tambah Data Bagian
@endsection


@section('content')
<div>
        <form action="/bagian" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Bagian</label>
                <input type="text" class="form-control" name="nama_bagian" id="title" placeholder="Masukkan Nama Bagian">
                @error('nama_bagian')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
</div>
@endsection