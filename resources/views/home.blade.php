@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome in my Sistem</div>

                <div class="card-body mx-auto">
                    <h2>Selamat Datang {{ Auth::user()->profile->nama_lengkap }}</h2>
                    <div class="d-grid gap-2 col-6 mx-auto">
                        <a class="btn btn-primary" href="/profile" role="button">Admin Sistem</a>
                        
                      </div>
                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
