<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('/CSS/dist/img/'.Auth::user()->profile->foto.'')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <!-- tempat buat profil, foto profil jangan lupa di ganti -->
        <a href="#" class="d-block">{{ Auth::user()->profile->nama_lengkap }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/profile" class="nav-link">
            <i class="nav-icon fas fa-user-tie"></i>
            <p>
              Data Profile
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/instansi" class="nav-link">
            <i class="nav-icon far fa-calendar-alt"></i>
            <p>
              Instansi
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/bagian" class="nav-link">
            <i class="nav-icon far fa-image"></i>
            <p>
              Bagian
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/jenissurat" class="nav-link">
            <i class="nav-icon fas fa-columns"></i>
            <p>
              Jenis Surat
            </p>
          </a>
        </li>
        <li class="nav-header">Mail</li>
        <li class="nav-item">
          <a href="/suratmasuk" class="nav-link">
            <i class="nav-icon far fa-envelope"></i>
            <p>
              Surat Masuk
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="suratkeluar" class="nav-link">
            <i class="nav-icon fa fa-inbox"></i>
            <p>
              Surat Keluar
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href= "/home">
            
           
           <i class=" nav-icon fas fa-sign-out-alt"></i>
            <p>
              Sign Out
            </p>
          </a>
        </li>
  
    </nav>
    <!-- /.sidebar-menu -->
  </div>