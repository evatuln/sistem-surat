@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i>  Data Jenis Surat
@endsection

@section('judul')
    
<a href="{{ route('jenissurat.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i> Tambah</a>
@endsection


@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success') }}
    </div>
@endif

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Jenis Surat</th>
                <th scope="col" >Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($jenissurat as $key=>$jenissurat)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$jenissurat->jenis_surat}}</td>
                        <td style="display: flex;">
                            <form action="/jenissurat/{{$jenissurat->id}}" method="POST">
                                <a href= " {{ route('jenissurat.show', ['jenissurat' => $jenissurat->id])}}" class="btn btn-info">Show</a>
                                <a href="/jenissurat/{{$jenissurat->id}}/edit" class="btn btn-primary">Edit</a>
                                
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection