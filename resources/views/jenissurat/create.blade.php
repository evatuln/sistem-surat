@extends('layout.master')

@section('title')
   Data Jenis Surat
@endsection

@section('judul')
    Tambah Data Jenis Surat
@endsection


@section('content')
<div>
        <form action="/jenissurat" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Jenis Surat</label>
                <input type="text" class="form-control" name="jenis_surat" id="title" placeholder="Masukkan Nama Jenis Surat">
                @error('jenis_surat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
</div>
@endsection