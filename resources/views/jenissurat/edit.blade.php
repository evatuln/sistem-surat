@extends('layout.master')

@section('title')
    Data Jenis Surat
@endsection

@section('judul')
    Edit Data dengan ID : {{$jenissurat->id}}
@endsection

@section('content')
<div>
        <form action="/jenissurat/{{$jenissurat->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Jenis Surat</label>
                <input type="text" class="form-control" value="{{$jenissurat->jenis_surat}}" name="jenis_surat" id="title" placeholder="Masukkan Jenis Surat">
                @error('jenis_surat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
</div>
@endsection