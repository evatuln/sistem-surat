@extends('layout.master')

@section('title')
    <i class="fas fa-user-tie"></i>  Data Surat Masuk
@endsection

@section('judul')
    
<a href="{{ route('suratmasuk.create') }}" class="btn btn-sm btn-primary float-right"><i class="fas fa-plus"></i> Tambah</a>
@endsection


@section('content')

@if(session('success'))
    <div class="alert alert-success">
        {{session('success') }}
    </div>
@endif

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Tanggal Surat</th>
                <th scope="col">Tanggal Terima</th>
                <th scope="col">Perihal</th>
                <th scope="col">ID Jenis</th>
                <th scope="col">ID Instansi</th>
                <th scope="col">ID Bagian</th>
                <th scope="col">ID User</th>
                <th scope="col">Dokumen</th>
                <th scope="col" >Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($suratmasuk as $key=>$suratmasuk)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$suratmasuk->tgl_surat}}</td>
                        <td>{{$suratmasuk->tgl_terima}}</td>
                        <td>{{$suratmasuk->perihal}}</td>
                        <td>{{$suratmasuk->jenis_id}}</td>
                        <td>{{$suratmasuk->instansi_id}}</td>
                        <td>{{$suratmasuk->bagian_id}}</td>
                        <td>{{$suratmasuk->user_id}}</td>
                        <td>{{$suratmasuk->dokumen}}</td>
                        <td style="display: flex;">
                            <form action="/suratmasuk/{{$suratmasuk->id}}" method="POST">
                                <a href= " {{ route('suratmasuk.show', ['suratmasuk' => $suratmasuk->id])}}" class="btn btn-info">Show</a>
                                <a href="/suratmasuk/{{$suratmasuk->id}}/edit" class="btn btn-primary">Edit</a>
                                
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger my-1" cast="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection