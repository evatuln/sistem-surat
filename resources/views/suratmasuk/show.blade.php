@extends('layout.master')

@section('title')
   Show : {{$suratmasuk->tgl_terima}}
@endsection

@section('judul')
    ID Data = {{$suratmasuk->perihal}}
@endsection


@section('content')
    <p> Tanggal Surat   : {{$suratmasuk->tgl_surat}}</p>
    <p> Tanggal Terima   : {{$suratmasuk->tgl_terima}}</p>
    <p> Perihal  : {{$suratmasuk->perihal}}</p>
    <p> ID Jenis   : {{$suratmasuk->jenis_id}}</p>
    <p> ID Instansi   : {{$suratmasuk->instansi_id}}</p>
    <p> ID Bagian   : {{$suratmasuk->bagian_id}}</p>
    <p> ID User   : {{$suratmasuk->user_id}}</p>
    <p> Dokumen   : {{$suratmasuk->dokumen}}</p>

@endsection 