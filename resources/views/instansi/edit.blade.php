@extends('layout.master')

@section('title')
    Data Instansi
@endsection

@section('judul')
    Edit Data dengan ID : {{$instansi->id}}
@endsection

@section('content')
<div>
        <form action="/instansi/{{$instansi->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama Instansi</label>
                <input type="text" class="form-control" value="{{$instansi->nama_instansi}}" name="nama_instansi" id="title" placeholder="Masukkan Nama Instansi">
                @error('nama_instansi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Simpan Edit</button>
        </form>
</div>
@endsection