@extends('layout.master')

@section('title')
   Data Instansi
@endsection

@section('judul')
    Tambah Data Instansi
@endsection


@section('content')
<div>
        <form action="/instansi" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Instansi</label>
                <input type="text" class="form-control" name="nama_instansi" id="title" placeholder="Masukkan Nama Instansi">
                @error('nama_instansi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah</button>
        </form>
</div>
@endsection