<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('profile', 'ProfileController');

Route::resource('bagian', 'BagianController');

Route::resource('jenissurat', 'JenissuratController');

Route::resource('instansi', 'InstansiController');

Route::resource('suratmasuk', 'SuratmasukController');

Route::resource('suratkeluar', 'SuratkeluarController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
